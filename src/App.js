import React from 'react';
import {Route, Switch} from "react-router-dom";
import ContactsApp from "./containers/ContactsApp/ContactsApp";
import AddFormContainer from "./containers/AddFormContainer/AddFormContainer";
import EditPage from "./containers/EditPage/EditPage";
import AppToolbar from "./components/Navigation/AppToolbar/AppToolbar";

const App = () => {
    return (
        <>
            <AppToolbar/>
            <Switch>
                <Route exact path="/" component={ContactsApp}/>
                <Route path="/editPage/:id" component={EditPage}/>
                <Route path="/addNewContact" component={AddFormContainer}/>
            </Switch>
        </>
    );
};

export default App;