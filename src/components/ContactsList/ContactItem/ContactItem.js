import React from 'react';
import {Card, makeStyles} from "@material-ui/core";

const useStyles = makeStyles({
    cardItem: {
        padding: "30px 10px 10px 10px",
        width: "200px",
        marginBottom: "25px",
        display: "flex",
        flexDirection: "column",
        cursor: "pointer"
    },
    imgDiv: {
        width: "150px",
        height: "150px",
        alignSelf: "center",
    },
    title: {
        textAlign: "center",
        textTransform: "capitalize"
    }
})

const ContactItem = ({photo, name, onClick}) => {
    const classes = useStyles();

    return (
        <Card
            className={classes.cardItem}
            onClick={onClick}
        >
            <div className={classes.imgDiv}>
                <img
                    src={photo}
                    alt=""
                    style={{width: "100%", textAlign: "center"}}
                />
            </div>
            <h4 className={classes.title}>{name}</h4>
        </Card>
    );
};

export default ContactItem;