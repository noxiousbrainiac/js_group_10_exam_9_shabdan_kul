import React from 'react';
import ContactItem from "./ContactItem/ContactItem";
import {makeStyles} from "@material-ui/core";

const useStyles = makeStyles({
    list: {
        paddingTop: "20px",
        display: "flex",
        flexWrap: "wrap",
        justifyContent: "space-evenly"
    }
})

const ContactsList = ({contacts, onClick}) => {
    const classes = useStyles();

    return (
        <div className={classes.list}>
            {
                contacts
                    ? Object.keys(contacts).map(con => (
                        <ContactItem
                            key={con}
                            name={contacts[con].name}
                            photo={contacts[con].photo}
                            onClick={()=> onClick(contacts[con], con)}
                        />
                    ))
                    : <h1 style={{textAlign: "center"}}>No contacts here</h1>
            }
        </div>
    );
};

export default ContactsList;