import React from 'react';
import {Backdrop, Button, Card, Fade, makeStyles, Modal} from "@material-ui/core";

const useStyles = makeStyles({
    card: {
        width: "400px",
        position: "absolute",
        left: "50%",
        top: "50%",
        transform: "translate(-50%, -50%)",
        padding: "20px"
    },
    imgDiv: {
        width: "150px",
        height: "150px",
        alignSelf: "center",
        marginRight: "30px"
    },
    btn: {
       marginRight: "20px"
    },
    cardContent: {
        display: "flex",
        marginBottom: "20px"
    }
})

const ModalWindow = ({close, open, photo, name, phone, email, remove, toEdit}) => {
    const classes = useStyles();

    return (
        <Modal
            open={open}
            onClose={close}
            closeAfterTransition
            BackdropComponent={Backdrop}
            BackdropProps={{
                timeout: 500,
            }}
        >
            <Fade in={open}>
                <Card className={classes.card}>
                    <div className={classes.cardContent}>
                        <div className={classes.imgDiv}>
                            <img
                                src={photo}
                                alt=""
                                style={{width: "100%", textAlign: "center"}}
                            />
                        </div>
                        <div>
                            <p><b>Name:</b> {name}</p>
                            <p><b>Phone:</b> <a href={`tel:${phone}`}>{phone}</a></p>
                            <p><b>Email:</b> <a href={`mailto:${email}`}>{email}</a></p>
                        </div>
                    </div>
                    <div>
                        <Button
                            className={classes.btn}
                            variant="contained"
                            onClick={toEdit}
                        >
                            Edit
                        </Button>
                        <Button
                            className={classes.btn}
                            variant="contained"
                            onClick={remove}
                        >
                            Delete
                        </Button>
                    </div>
                </Card>
            </Fade>
        </Modal>
    );
};

export default ModalWindow;