import React from 'react';
import {Button, Card, Container, makeStyles, TextField} from "@material-ui/core";

const useStyles = makeStyles({
    formItems: {
        display: "flex",
        flexDirection: "column",
        justifyContent: "space-evenly",
        padding: "20px 0"
    },
    formItem: {
        marginBottom: "15px"
    },
    btn: {
        marginRight: "20px"
    },
    btnS: {
        display: "flex"
    },
    imgDiv: {
        width: "200px",
        height: "200px",
        alignSelf: "center"
    }
})

const Form = ({submit, inputHandle, input, backClick}) => {
    const classes = useStyles();
    const unknownUser = "https://cdn-icons-png.flaticon.com/512/3143/3143082.png";

    return (
        <Container>
            <form onSubmit={(e) => submit(e)}>
                <div className={classes.formItems}>
                    <TextField
                        className={classes.formItem}
                        label="Name"
                        name="name"
                        value={input.name}
                        onChange={(e) => inputHandle(e)}
                    />
                    <TextField
                        className={classes.formItem}
                        label="Phone"
                        name="phone"
                        value={input.phone}
                        onChange={(e) => inputHandle(e)}
                    />
                    <TextField
                        className={classes.formItem}
                        label="Email"
                        name="email"
                        value={input.email}
                        onChange={(e) => inputHandle(e)}
                    />
                    <TextField
                        className={classes.formItem}
                        label="Photo"
                        name="photo"
                        value={input.photo}
                        onChange={(e) => inputHandle(e)}
                    />
                    <Card className={classes.imgDiv}>
                        <img
                            src={input.photo
                                ? input.photo
                                : unknownUser
                            }
                            alt=""
                            style={{width: "100%", height: "100%"}}
                        />
                    </Card>
                    <div className={classes.btnS}>
                        <Button
                            className={classes.btn}
                            variant="contained"
                            color="primary"
                            type="submit"
                        >
                            Save
                        </Button>
                        <Button
                            className={classes.btn}
                            variant="contained"
                            color="primary"
                            onClick={() => backClick()}
                        >
                            Back to contacts
                        </Button>
                    </div>
                </div>
            </form>
        </Container>
    );
};

export default Form;