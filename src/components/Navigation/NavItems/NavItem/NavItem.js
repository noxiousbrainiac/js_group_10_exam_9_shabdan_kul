import React from 'react';
import {NavLink} from "react-router-dom";
import {makeStyles} from "@material-ui/core";

const useStyles = makeStyles({
    li: {
        marginLeft: "10px",
    },
    navItem: {
        textDecoration: "none",
        textTransform: "uppercase",
        fontWeight: "bold",
        color: "beige",
        "&:hover": {
            color: "black"
        }
    }
})

const NavItem = ({to, exact, children}) => {
    const classes = useStyles();

    return (
        <li className={classes.li}>
            <NavLink className={classes.navItem} exact={exact} to={to}>{children}</NavLink>
        </li>
    );
};

export default NavItem;