import React from 'react';
import {makeStyles} from "@material-ui/core";
import NavItem from "./NavItem/NavItem";

const useStyles = makeStyles({
    list: {
        display: "flex",
        listStyle: "none"
    }
})

const NavItems = () => {
    const classes = useStyles();

    return (
        <ul className={classes.list}>
            <NavItem to="/addNewContact" >Add new contact</NavItem>
        </ul>
    );
};

export default NavItems;