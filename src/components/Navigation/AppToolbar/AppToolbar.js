import React from 'react';
import {makeStyles, Toolbar} from "@material-ui/core";
import {NavLink} from "react-router-dom";
import NavItems from "../NavItems/NavItems";

const useStyle = makeStyles({
    Toolbar: {
        background: "gray",
        justifyContent: "space-between"
    },
    title: {
        color: "beige",
        margin: "0 0 0 5px"
    },
    logoImg: {
        width: "100%"
    },
    logo: {
        display: "flex",
        alignItems: "center",
        textDecoration: "none"
    }
})
const AppToolbar = () => {
    const classes = useStyle();

    return (
        <Toolbar className={classes.Toolbar}>
            <NavLink to="/" className={classes.logo}>
                <div style={{width: "40px"}}>
                    <img className={classes.logoImg} src="https://cdn-icons-png.flaticon.com/512/46/46646.png" alt=""/>
                </div>
                <h3 className={classes.title}>Contacts</h3>
            </NavLink>
            <NavItems/>
        </Toolbar>
    );
};

export default AppToolbar;