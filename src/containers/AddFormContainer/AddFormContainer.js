import React, {useState} from 'react';
import {useDispatch} from "react-redux";
import {useHistory} from "react-router-dom";
import {postContact} from "../../store/actions";
import Form from "../../components/UI/Form/Form";

const AddFormContainer = () => {
    const dispatch = useDispatch();
    const history = useHistory();
    const [input, setInput] = useState({
        name: "",
        phone: "",
        email: "",
        photo: "",
    });

    const inputHandle = e => {
        const {name, value} = e.target;
        setInput(prevState => ({
            ...prevState,
            [name]: value
        }))
    }

    const postData = async (event) => {
        event.preventDefault();
        try {
            await dispatch(postContact(input));
            history.push("/");
        } catch (error) {
            console.log(error);
        }
    }

    const backToContacts = () => {
        history.push("/");
    }

    return <Form inputHandle={inputHandle} input={input} backClick={backToContacts} submit={postData}/>;
};

export default AddFormContainer;