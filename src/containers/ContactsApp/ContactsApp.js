import React, {useEffect, useState} from 'react';
import {Container, LinearProgress} from "@material-ui/core";
import ContactsList from "../../components/ContactsList/ContactsList";
import {useDispatch, useSelector} from "react-redux";
import {getContacts, removeContact} from "../../store/actions";
import {useHistory} from "react-router-dom";
import ModalWindow from "../../components/UI/ModalWindow/ModalWindow";

const ContactsApp = () => {
    const dispatch = useDispatch();
    const history = useHistory();
    const [openModal, setOpenModal] = useState(false);
    const [contact, setContact] = useState({});
    const [idItem, setIdItem] = useState("");
    const {contacts, loading, getError} = useSelector(state => ({
        contacts: state.contacts,
        loading: state.loading,
        getError: state.getError
    }));

    const showContact = (contact, id) => {
        setContact(contact);
        setIdItem(id);
        setOpenModal(true);
    }

    const handleClose = () => setOpenModal(false);

    const toEditPage = () => {
        history.push(`/editPage/${idItem}`);
    }

    const deleteContact = async () => {
        await dispatch(removeContact(idItem));
        await dispatch(getContacts());
        setOpenModal(false)
    }

    useEffect(() => {
        dispatch(getContacts());
    }, [dispatch]);

    let ContactsContainer = (
        <Container>
            <ContactsList contacts={contacts} onClick={showContact}/>
            <ModalWindow
                open={openModal}
                close={handleClose}
                photo={contact.photo}
                name={contact.name}
                email={contact.email}
                phone={contact.phone}
                toEdit={toEditPage}
                remove={deleteContact}
            />
        </Container>
    );

    if (loading) {
        ContactsContainer = (
            <Container>
                <LinearProgress
                    style={{
                        position: "absolute",
                        left: "50%",
                        top: "40%",
                        transform: "translateX(-50%)",
                        width: "300px",
                        height: "20px",
                    }}
                />
            </Container>
        );
    } else if (getError) {
        ContactsContainer = <h1 style={{textAlign: "center"}}>Something wrong!</h1>;
    }

    return ContactsContainer;
};

export default ContactsApp;