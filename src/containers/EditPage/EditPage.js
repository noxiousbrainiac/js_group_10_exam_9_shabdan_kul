import React, {useEffect, useState} from 'react';
import {Container, LinearProgress} from "@material-ui/core";
import {useHistory, useParams} from "react-router-dom";
import {useDispatch, useSelector} from "react-redux";
import {edit, getOneContact} from "../../store/actions";
import Form from "../../components/UI/Form/Form";

const EditPage = () => {
    const history = useHistory();
    const params = useParams();
    const dispatch = useDispatch();
    const {contact, loading, contactError} = useSelector(state => ({
        contact: state.contact,
        loading: state.loading,
        contactError: state.contactError
    }));

    const [input, setInput] = useState({
        name: "",
        phone: "",
        email: "",
        photo: "",
    });

    const inputHandle = e => {
        const {name, value} = e.target;
        setInput(prevState => ({
            ...prevState,
            [name]: value
        }))
    }

    const backToHomePage = () => {
        history.push("/");
    }

    const saveData = async (e) => {
        e.preventDefault();
        try {
            await dispatch(edit(input, params.id));
            history.push(`/`);
        } catch (e) {
            console.log(e);
        }
    }

    useEffect(() => {
        dispatch(getOneContact(params.id));
        contact.name && setInput(prev => ({
            ...prev,
            name: contact.name,
            phone: contact.phone,
            email: contact.email,
            photo: contact.photo
        }));

    }, [dispatch, params.id, contact.name, contact.phone, contact.email, contact.photo]);

    let EditPageContainer = (
        <Form submit={saveData} inputHandle={inputHandle} input={input} backClick={backToHomePage}/>
    )

    if (loading) {
        EditPageContainer = (
            <Container>
                <LinearProgress
                    style={{
                        position: "absolute",
                        left: "50%",
                        top: "40%",
                        transform: "translateX(-50%)",
                        width: "300px",
                        height: "20px"
                    }}
                />
            </Container>
        );
    } else if (contactError) {
        EditPageContainer = <h1 style={{textAlign: "center"}}>Something wrong!</h1>;
    }

    return EditPageContainer;
};

export default EditPage;