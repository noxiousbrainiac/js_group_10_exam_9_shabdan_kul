import axiosApi from "../axiosApi";

export const POST_CONTACT_REQUEST = "POST_CONTACT_REQUEST";
export const POST_CONTACT_SUCCESS = "POST_CONTACT_SUCCESS";
export const POST_CONTACT_FAILURE = "POST_CONTACT_FAILURE";

export const GET_CONTACTS_SUCCESS = "GET_CONTACTS_SUCCESS";
export const GET_CONTACTS_REQUEST = "GET_CONTACTS_REQUEST";
export const GET_CONTACTS_FAILURE = "GET_CONTACTS_FAILURE";

export const REMOVE_CONTACT_SUCCESS = "REMOVE_CONTACT_SUCCESS";
export const REMOVE_CONTACT_REQUEST = "REMOVE_CONTACT_REQUEST";
export const REMOVE_CONTACT_FAILURE = "REMOVE_CONTACT_FAILURE";

export const GET_CONTACT_SUCCESS = "GET_CONTACT_SUCCESS";
export const GET_CONTACT_REQUEST = "GET_CONTACT_REQUEST";
export const GET_CONTACT_FAILURE = "GET_CONTACT_FAILURE";

export const PUT_CONTACT_SUCCESS = "PUT_CONTACT_SUCCESS";
export const PUT_CONTACT_REQUEST = "PUT_CONTACT_REQUEST";
export const PUT_CONTACT_FAILURE = "PUT_CONTACT_FAILURE";

export const postContactRequest = () => ({type: POST_CONTACT_REQUEST});
export const postContactSuccess = contact => ({type: POST_CONTACT_SUCCESS, payload: contact});
export const postContactFailure = error => ({type: POST_CONTACT_FAILURE, payload: error});

export const getContactsSuccess = (data, loading) => ({type: GET_CONTACTS_SUCCESS, payload: data, loading});
export const getContactsRequest = (loading) => ({type: GET_CONTACTS_REQUEST, payload: loading});
export const getContactsFailure = (e) => ({type: GET_CONTACTS_FAILURE, payload: e });

export const removeContactSuccess = () => ({type: REMOVE_CONTACT_SUCCESS});
export const removeContactRequest = () => ({type: REMOVE_CONTACT_REQUEST});
export const removeContactFailure = error => ({type: REMOVE_CONTACT_FAILURE, payload: error});

export const getOneContactSuccess = (data, loading) => ({type: GET_CONTACT_SUCCESS, payload: data, loading});
export const getOneContactRequest = (loading) => ({type: GET_CONTACT_REQUEST, payload: loading});
export const getOneContactFailure = e => ({type: GET_CONTACT_FAILURE, payload: e});

export const putContactSuccess = () => ({type: PUT_CONTACT_SUCCESS});
export const putContactRequest = () => ({type: PUT_CONTACT_REQUEST});
export const putContactFailure = e => ({type:PUT_CONTACT_FAILURE, payload: e});

export const postContact = contact => async (dispatch) => {
    try {
        dispatch(postContactRequest());
        await axiosApi.post(`/contacts.json`, contact);
        dispatch(postContactSuccess(contact));
    } catch (e) {
        dispatch(postContactFailure(e));
        throw e;
    }
}

export const getContacts = () => async (dispatch) => {
    try {
        dispatch(getContactsRequest());
        const {data} = await axiosApi.get(`/contacts.json`);
        dispatch(getContactsSuccess(data));
    } catch (e) {
        dispatch(getContactsFailure(e));
        throw e;
    }
}

export const removeContact = (contact) => async (dispatch) => {
    try {
        dispatch(removeContactRequest());
        await axiosApi.delete(`/contacts/${contact}.json`);
        dispatch(removeContactSuccess());
    } catch (e) {
        dispatch(removeContactFailure(e));
        throw e;
    }
}

export const getOneContact = (id) => async (dispatch) => {
    try {
        dispatch(getOneContactRequest());
        const {data} = await axiosApi.get(`/contacts/${id}.json`);
        dispatch(getOneContactSuccess(data));
    } catch (e) {
        dispatch(getOneContactFailure(e));
        throw e;
    }
}

export const edit = (data, id) => async (dispatch) => {
    try {
        dispatch(putContactRequest());
        await axiosApi.put(`/contacts/${id}.json`, data);
        dispatch(putContactSuccess());
    } catch (e) {
        dispatch(putContactFailure(e));
        throw e;
    }
}