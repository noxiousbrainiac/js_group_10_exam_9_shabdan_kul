import {
    GET_CONTACT_FAILURE,
    GET_CONTACT_REQUEST,
    GET_CONTACT_SUCCESS,
    GET_CONTACTS_FAILURE,
    GET_CONTACTS_REQUEST,
    GET_CONTACTS_SUCCESS,
    POST_CONTACT_FAILURE,
    POST_CONTACT_REQUEST,
    POST_CONTACT_SUCCESS, PUT_CONTACT_FAILURE,
    PUT_CONTACT_REQUEST,
    PUT_CONTACT_SUCCESS,
    REMOVE_CONTACT_FAILURE,
    REMOVE_CONTACT_REQUEST,
    REMOVE_CONTACT_SUCCESS
} from "./actions";

const initialState = {
    contacts: [],
    contact: {},
    error: null,
    getError: null,
    loading: false,
    removeError: null,
    contactError: null,
    putError: null
};

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case POST_CONTACT_REQUEST:
            return {...state};
        case POST_CONTACT_SUCCESS:
            return {...state, contacts: action.payload};
        case POST_CONTACT_FAILURE:
            return {...state, error: action.payload};
        case GET_CONTACTS_SUCCESS:
            return {...state, contacts: action.payload, loading: false};
        case GET_CONTACTS_REQUEST:
            return {...state, loading: true};
        case GET_CONTACTS_FAILURE:
            return {...state, getError: action.payload, loading: false}
        case REMOVE_CONTACT_SUCCESS:
            return {...state};
        case REMOVE_CONTACT_REQUEST:
            return {...state};
        case REMOVE_CONTACT_FAILURE:
            return {...state, removeError: action.payload};
        case GET_CONTACT_SUCCESS:
            return {...state, contact: action.payload, loading: false};
        case GET_CONTACT_REQUEST:
            return {...state, loading: true};
        case GET_CONTACT_FAILURE:
            return {...state, contactError: action.payload}
        case PUT_CONTACT_SUCCESS:
            return {...state};
        case PUT_CONTACT_REQUEST:
            return {...state};
        case PUT_CONTACT_FAILURE:
            return {...state, putError: action.payload};
        default:
            return state;
    }
}

export default reducer;